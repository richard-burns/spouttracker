#include "ofApp.h"
#include <string>

//--------------------------------------------------------------
void ofApp::setup(){
	ofSetFrameRate(60);
	//image.load("people.jpg");
	image.grabScreen(0,0,1280,720);
	spoutReceiver.setup();
	finder.setup("haarcascade.xml");
	settings.loadFile("settings.xml");
	string host = settings.getValue("settings:host", "localhost");
	int port = settings.getValue("settings:port", 8000);
	minimumareasize = settings.getValue("settings:minimumareasize", 62500);
	finder.findHaarObjects(image);
	sender.setup(host, port);

}

//--------------------------------------------------------------
void ofApp::update(){
	image.grabScreen(0, 0, 1280, 720);
	finder.findHaarObjects(image);

}

//--------------------------------------------------------------
void ofApp::draw(){
	ofClear(0, 0, 0);

	spoutReceiver.updateTexture();
	spoutReceiver.getTexture().draw(0, 0);
	ofNoFill();

	int blobstracked = 0;

	for (unsigned int i = 0; i < finder.blobs.size(); i++) {
		ofRectangle cur = finder.blobs[i].boundingRect;
		int area = cur.width*cur.height;

		if (area > minimumareasize) {
			blobstracked = blobstracked + 1;
			ofDrawRectangle(cur.x, cur.y, cur.width, cur.height);
			ofxOscMessage x;
			ofxOscMessage y;
			ofxOscMessage w;
			ofxOscMessage h;
			std::string s = std::to_string(i);

			x.setAddress("/blob" + s+"x");
			y.setAddress("/blob" + s + "y");
			w.setAddress("/blob" + s + "w");
			h.setAddress("/blob" + s + "h");

			x.addIntArg(cur.x);
			y.addIntArg(cur.y);
			w.addIntArg(cur.width);
			h.addIntArg(cur.height);

			sender.sendMessage(x, false);
			sender.sendMessage(y, false);
			sender.sendMessage(w, false);
			sender.sendMessage(h, false);

		}
	}

	ofxOscMessage b;
	b.setAddress("/blobstracked");
	b.addIntArg(blobstracked);
	sender.sendMessage(b, false);


}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

}

void ofApp::exit() {
	spoutReceiver.exit();
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
	if (button == 2) { // rh button
					   // Open the sender selection panel
					   // Spout must have been installed
		spoutReceiver.showSenders();
	}

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
